﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService.EFModel.DatabaseTables
{
    public class Address
    {
        public virtual int PK_AddressID { get; set; }

        public virtual string Address1 { get; set; }

        public virtual string Address2 { get; set; }

        public virtual string Address3 { get; set; }

        public virtual string City { get; set; }

        public virtual string County { get; set; }

        public virtual string PostCode { get; set; }

    }
}
