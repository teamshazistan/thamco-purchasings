﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService.EFModel.DatabaseTables
{
    public class Purchases
    {
        //Table Index
        public virtual int PK_PurchaseID { get; set; }

        //P.O.# for invoices. Format: P########
        public virtual string PurchaseNo { get; set; }

        //CRN of the item purchased
        public virtual string ItemCRN { get; set; }

        //Name of the item purchased
        public virtual string ItemName { get; set; }

        //Description of the item purchased
        public virtual string ItemDesc { get; set; }

        //The quantity of items purchased by the customers.
        public virtual int ItemQuantity { get; set; }

        //The price of each item unit (£)
        public virtual double ItemUnitPrice { get; set; }

        //Not customer table index. Unique customer number for e.g (C4049754)
        public virtual string CustomerID { get; set; }

        //The FK of the invoice associated with the current customer purchase.
        public virtual int FK_InvoiceID { get; set; }
        //The invoice associated with the current customer purchase.
        public virtual Invoice PurchaseInvoice { get; set; }
    }
}
