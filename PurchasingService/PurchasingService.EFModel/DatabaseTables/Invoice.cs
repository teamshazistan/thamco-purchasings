﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService.EFModel.DatabaseTables
{
    public class Invoice
    {
        //The primary key of the generated invoice
        public virtual int PK_InvoiceID { get; set; }

        //The invoice number of the generated invoice formatted (I########)
        public virtual string InvoiceNo { get; set; }

        //The date and time the invoice was generated.
        public virtual DateTime InvoiceDate { get; set; }

        //Relationships

        //The FK of the billing address associated with the invoice.
        public virtual int FK_BillingAddressID { get; set; }

        //The billing address object associated with the invoice.
        public virtual Address BillingAddress { get; set; }


        //The FK of the shipping address associated with the invoice.
        public virtual int FK_ShippingAddressID { get; set; }

        //The shipping address object associated with the invoice.
        public virtual Address ShippingAddress { get; set; }

        //A list of all the items purchased as part of this invoice.
        public virtual List<Purchases> PurchasesList { get; set; }
    }
}
