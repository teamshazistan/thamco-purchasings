﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService.EFModel.DatabaseTables
{
    public class PurchasingDb : DbContext, IDisposable
    {
        public DbSet<Purchases> Purchases { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Address> Address { get; set; }

        public PurchasingDb() : base("name=PurchasingService.PurchasingDb")
        {
        }

        static PurchasingDb()
        {
            System.Data.Entity.Database.SetInitializer(new DbInitializer());
        }

        public class DbIntializer: CreateDatabaseIfNotExists<PurchasingDb>
        {

        }
    }
}
