﻿using PurchasingService_Repository.DTOs.Invoices;
using PurchasingService_Repository.DTOs.Orders;
using PurchasingService_Repository.EFClones;
using PurchasingService_Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace PurchasingService_Repository.Repositories.Tests
{
    [TestFixture]
    public class StaffRepositoryTests
    {
        [TestCase]
        public void GetOrdersFilteredTest()
        {
            //Arrange
            AbstractRepository repo = new StaffRepository();

            //Act
            var result1 = ((StaffRepository)repo).GetOrdersFiltered("P00000001", "C00000001");
            var result2 = ((StaffRepository)repo).GetOrdersFiltered("", "C00000001");
            var result3 = ((StaffRepository)repo).GetOrdersFiltered("P00000001", "");
            var result4 = ((StaffRepository)repo).GetOrdersFiltered(null, null);
            var result5 = ((StaffRepository)repo).GetOrdersFiltered("BADCODE1", "BADCODE2");

            //Assert
            Assert.IsNotNull(result1);
            Assert.IsInstanceOf(typeof(IEnumerable<_Orders>), result1);
            Assert.IsTrue(result1.Count() > 0);

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(IEnumerable<_Orders>), result2);
            Assert.IsTrue(result2.Count() > 0);

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(IEnumerable<_Orders>), result3);
            Assert.IsTrue(result3.Count() > 0);

            Assert.IsNotNull(result4); //For customer repo, this would fail as we do not want customers seeing all orders. However, the staff implementation of this function
            Assert.IsInstanceOf(typeof(IEnumerable<_Orders>), result4); //Allows staff to retrieve all orders.
            Assert.IsTrue(result4.Count() > 0);

            Assert.IsNotNull(result5);
            Assert.IsInstanceOf(typeof(IEnumerable<_Orders>), result5);
            Assert.IsTrue(result5.Count() == 0);
        }

        [TestCase]
        public void UpdateOrderStatusTest()
        {
            //Arrange
            AbstractRepository repo = new StaffRepository();

            OrderStatusUpdateDTO update1 = new OrderStatusUpdateDTO() { PurchaseOrderNo = "P00000001", NewStatusCode = 5 };
            OrderStatusUpdateDTO update2 = new OrderStatusUpdateDTO() { PurchaseOrderNo = "P00000001", NewStatusCode = 6 }; //Status code 6 does not exist. Should return null.
            OrderStatusUpdateDTO update3 = new OrderStatusUpdateDTO();
            OrderStatusUpdateDTO update4 = new OrderStatusUpdateDTO() { NewStatusCode = 2 };
            OrderStatusUpdateDTO update5 = new OrderStatusUpdateDTO() { PurchaseOrderNo = "PER£$!!$!", NewStatusCode = 1 }; 

            //Act
            var result1 = ((StaffRepository)repo).UpdateOrderStatus(update1);
            var result2 = ((StaffRepository)repo).UpdateOrderStatus(update2);
            var result3 = ((StaffRepository)repo).UpdateOrderStatus(update3);
            var result4 = ((StaffRepository)repo).UpdateOrderStatus(update4);
            var result5 = ((StaffRepository)repo).UpdateOrderStatus(update5);
            var result6 = ((StaffRepository)repo).UpdateOrderStatus(null);

            //Assert
            Assert.IsNotNull(result1);
            Assert.IsInstanceOf(typeof(_Orders), result1);
            Assert.IsTrue(result1.PK_OrderID != 0);
            Assert.IsTrue(result1.PurchaseOrderNo == update1.PurchaseOrderNo);
            Assert.IsTrue(result1.FK_StatusCode == update1.NewStatusCode);

            Assert.IsNull(result2);

            Assert.IsNull(result3);

            Assert.IsNull(result4);

            Assert.IsNull(result5);
        }

        [TestCase]
        public void GetInvoicesFilteredTest()
        {
            //Arrange
            AbstractRepository repo = new StaffRepository();

            //Act
            var result1 = ((StaffRepository)repo).GetInvoicesFiltered("P00000001", "I00000001", "C00000001");
            var result2 = ((StaffRepository)repo).GetInvoicesFiltered("", "", "C00000001");
            var result3 = ((StaffRepository)repo).GetInvoicesFiltered("P00000001", "", "C00000001");
            var result4 = ((StaffRepository)repo).GetInvoicesFiltered("P00000001", "", "");
            var result5 = ((StaffRepository)repo).GetInvoicesFiltered(null, null, null);
            var result6 = ((StaffRepository)repo).GetInvoicesFiltered("BADCODE1", "BADCODE2", "BADCODE3");

            //Assert
            Assert.IsNotNull(result1);
            Assert.IsInstanceOf(typeof(IEnumerable<_Invoices>), result1);
            Assert.IsTrue(result1.Count() > 0);

            Assert.IsNotNull(result2); //Will not return null for staff as staff are allowed to view all invoices no matter if a customer number is provided.
            Assert.IsInstanceOf(typeof(IEnumerable<_Invoices>), result2);
            Assert.IsTrue(result2.Count() > 0);

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(IEnumerable<_Invoices>), result3);
            Assert.IsTrue(result3.Count() > 0);

            Assert.IsNotNull(result4); //Will not return null for staff as staff are allowed to view all invoices no matter if a customer number is provided.
            Assert.IsInstanceOf(typeof(IEnumerable<_Invoices>), result4);
            Assert.IsTrue(result4.Count() > 0);

            Assert.IsNotNull(result5); //Will not return null for staff as staff are allowed to view all invoices no matter if a customer number is provided.
            Assert.IsInstanceOf(typeof(IEnumerable<_Invoices>), result5);
            Assert.IsTrue(result5.Count() > 0);

            Assert.IsNotNull(result6);
            Assert.IsInstanceOf(typeof(IEnumerable<_Invoices>), result6);
            Assert.IsTrue(result6.Count() == 0);
        }

        [TestCase]
        public void GetPendingInvoicesTest()
        {
            //Arrange
            AbstractRepository repo = new StaffRepository();

            //Act
            var result1 = ((StaffRepository)repo).GetPendingInvoices();

            //Assert
            Assert.IsNotNull(result1);
            Assert.IsInstanceOf(typeof(IEnumerable<_Invoices>), result1);
            Assert.IsTrue(result1.Count() > 0);
        }

        [TestCase]
        public void UpdateInvoiceSentStatusTest()
        {
            //Arrange
            AbstractRepository repo = new StaffRepository();
            InvoiceStatusUpdateDTO update1 = new InvoiceStatusUpdateDTO() { InvoiceNo = "I00000001", SentToCustomer = true };
            InvoiceStatusUpdateDTO update2 = new InvoiceStatusUpdateDTO() { SentToCustomer = true };
            InvoiceStatusUpdateDTO update3 = new InvoiceStatusUpdateDTO() { InvoiceNo = "I00000001" }; //Will not fail as SentToCustomer will default to false
            InvoiceStatusUpdateDTO update4 = new InvoiceStatusUpdateDTO() { InvoiceNo = "TEST", SentToCustomer = true };
            InvoiceStatusUpdateDTO update5 = new InvoiceStatusUpdateDTO();

            //Act
            var result1 = ((StaffRepository)repo).UpdateInvoiceSentStatus(update1);
            var result2 = ((StaffRepository)repo).UpdateInvoiceSentStatus(update2);
            var result3 = ((StaffRepository)repo).UpdateInvoiceSentStatus(update3);
            var result4 = ((StaffRepository)repo).UpdateInvoiceSentStatus(update4);
            var result5 = ((StaffRepository)repo).UpdateInvoiceSentStatus(update5);
            var result6 = ((StaffRepository)repo).UpdateInvoiceSentStatus(null);

            //Assert
            Assert.IsNotNull(result1);
            Assert.IsInstanceOf(typeof(_Invoices), result1);
            Assert.IsTrue(result1.InvoiceNo == update1.InvoiceNo);
            Assert.IsTrue(result1.SentToCustomer == update1.SentToCustomer);

            Assert.IsNull(result2);

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(_Invoices), result3);
            Assert.IsTrue(result3.InvoiceNo == update3.InvoiceNo);
            Assert.IsTrue(result3.SentToCustomer == update3.SentToCustomer);

            Assert.IsNull(result4);

            Assert.IsNull(result5);

            Assert.IsNull(result6);

        }
    }
}