﻿using PurchasingService_Repository.DTOs.Orders.NewOrders;
using PurchasingService_Repository.EFClones;
using PurchasingService_Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace PurchasingService_Repository.Repositories.Tests
{
    [TestFixture]
    public class CustomerRepositoryTests
    {
        [TestCase]
        public void GetOrdersFilteredTest()
        {
            //Arrange
            AbstractRepository repo = new CustomerRepository();

            //Act
            var result1 = ((CustomerRepository)repo).GetOrdersFiltered("P00000001", "C00000001");
            var result2 = ((CustomerRepository)repo).GetOrdersFiltered("", "C00000001");
            var result3 = ((CustomerRepository)repo).GetOrdersFiltered("P00000001", "");
            var result4 = ((CustomerRepository)repo).GetOrdersFiltered(null, null);
            var result5 = ((CustomerRepository)repo).GetOrdersFiltered("BADCODE1", "BADCODE2");

            //Assert
            Assert.IsNotNull(result1);
            Assert.IsInstanceOf(typeof(IEnumerable<_Orders>), result1);
            Assert.IsTrue(result1.Count() > 0);

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(IEnumerable<_Orders>), result2);
            Assert.IsTrue(result2.Count() > 0);

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(IEnumerable<_Orders>), result3);
            Assert.IsTrue(result3.Count() > 0);

            Assert.IsNull(result4);

            Assert.IsNotNull(result5);
            Assert.IsInstanceOf(typeof(IEnumerable<_Orders>), result5);
            Assert.IsTrue(result5.Count() == 0);
        }

        [TestCase]
        public void GetInvoicesFilteredTest()
        {
            //Arrange
            AbstractRepository repo = new CustomerRepository();

            //Act
            var result1 = ((CustomerRepository)repo).GetInvoicesFiltered("P00000001", "I00000001", "C00000001");
            var result2 = ((CustomerRepository)repo).GetInvoicesFiltered("", "", "C00000001");
            var result3 = ((CustomerRepository)repo).GetInvoicesFiltered("P00000001", "", "C00000001");
            var result4 = ((CustomerRepository)repo).GetInvoicesFiltered("P00000001", "", "");
            var result5 = ((CustomerRepository)repo).GetInvoicesFiltered(null, null, null);
            var result6 = ((CustomerRepository)repo).GetInvoicesFiltered("BADCODE1", "BADCODE2", "BADCODE3");

            //Assert
            Assert.IsNotNull(result1);
            Assert.IsInstanceOf(typeof(IEnumerable<_Invoices>), result1);
            Assert.IsTrue(result1.Count() > 0);

            Assert.IsNull(result2);

            Assert.IsNotNull(result3);
            Assert.IsInstanceOf(typeof(IEnumerable<_Invoices>), result3);
            Assert.IsTrue(result3.Count() > 0);

            Assert.IsNull(result4);

            Assert.IsNull(result5);

            Assert.IsNotNull(result6);
            Assert.IsInstanceOf(typeof(IEnumerable<_Invoices>), result6);
            Assert.IsTrue(result6.Count() == 0);
        }

        [TestCase]
        public void PlaceOrderTest()
        {
            //Arrange
            AbstractRepository repo = new CustomerRepository();
            NewOrderAddressDTO testAddress1 = new NewOrderAddressDTO()
            {
                Address1 = "Address1",
                Address2 = "Address2",
                Address3 = "Address3",
                City = "City",
                Country = "Country",
                PostCode = "PostCode",
                County = "County"
            };

            NewOrderDTO newOrder1 = new NewOrderDTO()
            {
                CustomerNo = "C12345678",
                OrderedItems = new List<NewOrderedItemsDTO>()
                {
                    new NewOrderedItemsDTO() { ItemEAN = "TestCRN", ItemName = "TestName", ItemDesc = "TestDesc", ItemQuanity = 10, ItemUnitPrice = 10.0 }
                },
                ShippingAddress = testAddress1,
                BillingAddress = testAddress1
            };

            NewOrderDTO newOrder2 = new NewOrderDTO()
            {
                CustomerNo = "C90123456",
                OrderedItems = new List<NewOrderedItemsDTO>()
                {
                    new NewOrderedItemsDTO() { ItemEAN = "TestCRN2", ItemName = "TestName2", ItemDesc = "TestDesc2", ItemQuanity = 1, ItemUnitPrice = 5.0 }
                },
                ShippingAddress = testAddress1,
                BillingAddress = testAddress1
            };

            //Act
            var result1 = ((CustomerRepository)repo).PlaceOrder(newOrder1);
            var result2 = ((CustomerRepository)repo).PlaceOrder(newOrder2);
            var result3 = ((CustomerRepository)repo).PlaceOrder(null);

            //Assert
            Assert.IsNotNull(result1);
            Assert.IsInstanceOf(typeof(_Orders), result1);
            Assert.IsTrue(result1.PK_OrderID != 0);
            Assert.IsTrue(result1.CustomerNo == newOrder1.CustomerNo);
            Assert.IsTrue(result1.PurchaseOrderNo != "");

            Assert.IsNotNull(result2);
            Assert.IsInstanceOf(typeof(_Orders), result2);
            Assert.IsTrue(result2.PK_OrderID != 0);
            Assert.IsTrue(result2.CustomerNo == newOrder2.CustomerNo);
            Assert.IsTrue(result2.PurchaseOrderNo != "");

            Assert.IsNull(result3);
        }
    }
}