﻿using PurchasingService.Controllers;
using PurchasingService_Repository.DTOs.Invoices;
using PurchasingService_Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using NUnit.Framework;

namespace PurchasingService.Controllers.Tests
{
    [TestFixture]
    public class InvoicesControllerTests
    {
        [TestCase]
        public void GetInvoicesTest()
        {
            // Arrange
            AbstractController controller = new InvoicesController();

            // Act
            IHttpActionResult result1 = ((InvoicesController)controller).GetInvoices(); //Bad request, no parameters.
            IHttpActionResult result2 = ((InvoicesController)controller).GetInvoices("P00000001", "", "C00000001"); //Ok response, customer provided and one of purchase order number or invoice number.
            IHttpActionResult result3 = ((InvoicesController)controller).GetInvoices("", "I00000001", "C00000001"); //Ok response, customer provided and one of purchase order number or invoice number.
            IHttpActionResult result4 = ((InvoicesController)controller).GetInvoices("BADTEXT", "0", "1"); //Ok response, customer provided and one of purchase order number or invoice number.
            IHttpActionResult result5 = ((InvoicesController)controller).GetInvoices("", "I00000001", ""); //No customer number, so fail.

            var negResult1 = result1 as NegotiatedContentResult<string>; 
            var negResult2 = result2 as NegotiatedContentResult<List<InvoiceDTO>>;
            var negResult3 = result3 as NegotiatedContentResult<List<InvoiceDTO>>;
            var negResult4 = result4 as NegotiatedContentResult<string>;
            var negResult5 = result5 as NegotiatedContentResult<string>;

            Assert.IsNotNull(negResult1);
            Assert.AreEqual(HttpStatusCode.BadRequest, negResult1.StatusCode);

            Assert.IsNotNull(negResult2);
            Assert.AreEqual(HttpStatusCode.OK, negResult2.StatusCode);

            Assert.IsNotNull(negResult3);
            Assert.AreEqual(HttpStatusCode.OK, negResult3.StatusCode);

            Assert.IsNotNull(negResult4);
            Assert.AreEqual(HttpStatusCode.NotFound, negResult4.StatusCode);

            Assert.IsNotNull(negResult5);
            Assert.AreEqual(HttpStatusCode.BadRequest, negResult5.StatusCode);
        }

        [TestCase]
        public void GetPendingInvoicesTest()
        {
            // Arrange
            AbstractController controller = new InvoicesController();

            // Act
            IHttpActionResult result1 = ((InvoicesController)controller).GetPendingInvoices();

            var negResult1 = result1 as NegotiatedContentResult<List<InvoiceDTO>>;

            Assert.IsNotNull(negResult1);
            Assert.AreEqual(HttpStatusCode.OK, negResult1.StatusCode);
        }
    }
}