﻿using PurchasingService.Controllers;
using PurchasingService_Repository.DTOs.Orders;
using PurchasingService_Repository.DTOs.Orders.NewOrders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using NUnit.Framework;

namespace PurchasingService.Controllers.Tests
{
    [TestFixture]
    public class OrdersControllerTests
    {
        [TestCase]
        public void GetOrdersTest()
        {
            //Arrange
            AbstractController controller = new OrdersController();

            //Act
            IHttpActionResult result1 = ((OrdersController)controller).GetOrders(); //Bad request, no parameters.
            IHttpActionResult result2 = ((OrdersController)controller).GetOrders("P00000001", "C00000001"); //Ok response, customer provided and purchase number provided.
            IHttpActionResult result3 = ((OrdersController)controller).GetOrders("", "C00000001"); //Ok response, customer provided.
            IHttpActionResult result4 = ((OrdersController)controller).GetOrders("P00000001", ""); //Ok response, purchase number provided.
            IHttpActionResult result5 = ((OrdersController)controller).GetOrders("BADTEXT", "1"); //NotFound response.

            //Assert
            var negResult1 = result1 as NegotiatedContentResult<string>;
            var negResult2 = result2 as NegotiatedContentResult<List<OrdersDTO>>;
            var negResult3 = result3 as NegotiatedContentResult<List<OrdersDTO>>;
            var negResult4 = result4 as NegotiatedContentResult<List<OrdersDTO>>;
            var negResult5 = result5 as NegotiatedContentResult<string>;

            Assert.IsNotNull(negResult1);
            Assert.AreEqual(HttpStatusCode.BadRequest, negResult1.StatusCode);

            Assert.IsNotNull(negResult2);
            Assert.AreEqual(HttpStatusCode.OK, negResult2.StatusCode);

            Assert.IsNotNull(negResult3);
            Assert.AreEqual(HttpStatusCode.OK, negResult3.StatusCode);

            Assert.IsNotNull(negResult4);
            Assert.AreEqual(HttpStatusCode.OK, negResult4.StatusCode);

            Assert.IsNotNull(negResult5);
            Assert.AreEqual(HttpStatusCode.NotFound, negResult5.StatusCode);
        }

        [TestCase]
        public void AddNewOrderTest()
        {
            //Arrange
            AbstractController controller = new OrdersController();
            NewOrderAddressDTO testAddress1 = new NewOrderAddressDTO()
            {
                Address1 = "Address1",
                Address2 = "Address2",
                Address3 = "Address3",
                City = "City",
                Country = "Country",
                PostCode = "PostCode",
                County = "County"
            };

            NewOrderDTO newOrder1 = new NewOrderDTO()
            {
                CustomerNo = "C12345678",
                OrderedItems = new List<NewOrderedItemsDTO>()
                {
                    new NewOrderedItemsDTO() { ItemEAN = "TestCRN", ItemName = "TestName", ItemDesc = "TestDesc", ItemQuanity = 10, ItemUnitPrice = 10.0 }
                },
                ShippingAddress = testAddress1,
                BillingAddress = testAddress1
            };

            NewOrderDTO newOrder2 = new NewOrderDTO()
            {
                CustomerNo = "C90123456",
                OrderedItems = new List<NewOrderedItemsDTO>()
                {
                    new NewOrderedItemsDTO() { ItemEAN = "TestCRN2", ItemName = "TestName2", ItemDesc = "TestDesc2", ItemQuanity = 1, ItemUnitPrice = 5.0 }
                },
                ShippingAddress = testAddress1,
                BillingAddress = testAddress1
            };

            NewOrderDTO newOrder3 = new NewOrderDTO();
            //Act
            IHttpActionResult result1 = ((OrdersController)controller).AddNewOrder(newOrder1); //Created response,
            IHttpActionResult result2 = ((OrdersController)controller).AddNewOrder(newOrder2); //Created reponse
            IHttpActionResult result3 = ((OrdersController)controller).AddNewOrder(newOrder3); //BadRequest, will be accepted as a valid object, however will be unable to add record.
            IHttpActionResult result4 = ((OrdersController)controller).AddNewOrder(null); //BadRequest, no object passed in.

            var negResult1 = result1 as NegotiatedContentResult<OrdersDTO>;
            var negResult2 = result2 as NegotiatedContentResult<OrdersDTO>;
            var negResult3 = result3 as NegotiatedContentResult<string>;
            var negResult4 = result4 as NegotiatedContentResult<string>;

            //Assert
            Assert.IsNotNull(negResult1);
            Assert.AreEqual(HttpStatusCode.Created, negResult1.StatusCode);

            Assert.IsNotNull(negResult2);
            Assert.AreEqual(HttpStatusCode.Created, negResult2.StatusCode);

            Assert.IsNotNull(negResult3);
            Assert.AreEqual(HttpStatusCode.BadRequest, negResult3.StatusCode);

            Assert.IsNotNull(negResult4);
            Assert.AreEqual(HttpStatusCode.BadRequest, negResult4.StatusCode);
        }

        [TestCase]
        public void UpdateOrderStatusTest()
        {
            //Arrange
            AbstractController controller = new OrdersController();
            OrderStatusUpdateDTO newOrderStatus1 = new OrderStatusUpdateDTO() { PurchaseOrderNo = "P00000001", NewStatusCode = 2 };
            OrderStatusUpdateDTO newOrderStatus2 = new OrderStatusUpdateDTO() { PurchaseOrderNo = "P00000001" };
            OrderStatusUpdateDTO newOrderStatus3 = new OrderStatusUpdateDTO() { PurchaseOrderNo = "DSADSAD", NewStatusCode = 2 };
            OrderStatusUpdateDTO newOrderStatus4 = new OrderStatusUpdateDTO() { NewStatusCode = 2 };

            //Act
            IHttpActionResult result1 = ((OrdersController)controller).UpdateOrderStatus(newOrderStatus1); //Created response.
            IHttpActionResult result2 = ((OrdersController)controller).UpdateOrderStatus(newOrderStatus2); //NotFound reponse, no status code provided.
            IHttpActionResult result3 = ((OrdersController)controller).UpdateOrderStatus(newOrderStatus3); //NotFound, bad purchase order number provided.
            IHttpActionResult result4 = ((OrdersController)controller).UpdateOrderStatus(newOrderStatus4); //NotFound, no purchase order number provided.
            IHttpActionResult result5 = ((OrdersController)controller).UpdateOrderStatus(null); //BadRequest, no object passed in.

            var negResult1 = result1 as NegotiatedContentResult<OrdersDTO>;
            var negResult2 = result2 as NegotiatedContentResult<string>;
            var negResult3 = result3 as NegotiatedContentResult<string>;
            var negResult4 = result4 as NegotiatedContentResult<string>;
            var negResult5 = result5 as NegotiatedContentResult<string>;

            //Assert
            Assert.IsNotNull(negResult1);
            Assert.AreEqual(HttpStatusCode.Created, negResult1.StatusCode);

            Assert.IsNotNull(negResult2);
            Assert.AreEqual(HttpStatusCode.NotFound, negResult2.StatusCode);

            Assert.IsNotNull(negResult3);
            Assert.AreEqual(HttpStatusCode.NotFound, negResult3.StatusCode);

            Assert.IsNotNull(negResult4);
            Assert.AreEqual(HttpStatusCode.NotFound, negResult4.StatusCode);

            Assert.IsNotNull(negResult5);
            Assert.AreEqual(HttpStatusCode.BadRequest, negResult5.StatusCode);
        }
    }
}