﻿using PurchasingService_EFModel.Tables;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_EFModel
{
    public class PurchasesDb : DbContext, IDisposable
    {
        public DbSet<AddressInfo> AddressInfo { get; set; }
        public DbSet<OrderStatus> OrderStatus { get; set; }
        public DbSet<OrderedItems> OrderedItems { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<Invoices> Invoices { get; set; }

        public PurchasesDb() : base("name=PurchasingService.PurchasesDb")
        {
            System.Data.Entity.Database.SetInitializer(new DbInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<OrderedItems>().HasRequired(x => x.Order).WithMany(x => x.OrderedItems).HasForeignKey(x => x.FK_OrderID);

            modelBuilder.Entity<Orders>().HasRequired(x => x.OrderStatus).WithMany().HasForeignKey(x => x.FK_StatusCode);
            modelBuilder.Entity<Orders>().HasOptional(x => x.Invoice).WithMany(x => x.OrderList).HasForeignKey(x => x.FK_InvoiceID);
            modelBuilder.Entity<Orders>().HasRequired(x => x.ShippingAddress).WithMany().HasForeignKey(x => x.FK_ShippingAddressID).WillCascadeOnDelete(false);
            modelBuilder.Entity<Orders>().HasRequired(x => x.BillingAddress).WithMany().HasForeignKey(x => x.FK_BillingAddressID).WillCascadeOnDelete(false);
        }

        private class DbInitializer : DropCreateDatabaseAlways<PurchasesDb>
        {
            protected override void Seed(PurchasesDb context)
            {
                var statusCodes = new List<OrderStatus>()
                {
                    new OrderStatus { PK_StatusCode = 1, StatusDescription = "Order Placed" },
                    new OrderStatus { PK_StatusCode = 2, StatusDescription = "Billing"},
                    new OrderStatus { PK_StatusCode = 3, StatusDescription = "Dispatch Pending"},
                    new OrderStatus { PK_StatusCode = 4, StatusDescription = "Dispatched"},
                    new OrderStatus { PK_StatusCode = 5, StatusDescription = "Complete"},
                    new OrderStatus { PK_StatusCode = -99, StatusDescription = "Cancelled"}
                };
                statusCodes.ForEach(x => context.OrderStatus.Add(x));

                var billingAddress = new List<AddressInfo>()
                {
                    new AddressInfo { Address1 = "2 Linthorpe Way", Address2 = "Ingleby Barwick", City = "Doncaster", County = "South Yorkshire", Country = "United States", PostCode = "DG2 1OP" }
                };
                billingAddress.ForEach(x => context.AddressInfo.Add(x));

                var shippingAddress = new List<AddressInfo>()
                {
                    new AddressInfo { Address1 = "Gowland Way", Address2 = "Gowland Hall", City = "Gowlandton", County = "Gowlandshire", Country = "Gowlandstan", PostCode = "GO2 1LA" }
                };
                shippingAddress.ForEach(x => context.AddressInfo.Add(x));

                context.SaveChanges();

                var invoices = new List<Invoices>()
                {
                    new Invoices { InvoiceNo = "I00000001", InvoiceDate = DateTime.Now, SentToCustomer = true },
                    new Invoices { InvoiceNo = "I00000002", InvoiceDate = DateTime.Now, SentToCustomer = false }
                };
                invoices.ForEach(x => context.Invoices.Add(x));

                context.SaveChanges();


                var purchases = new List<Orders>()
                {
                    new Orders { PurchaseOrderNo = "P00000001", CustomerNo = "C00000001", Invoice = invoices[0],  OrderStatus = statusCodes[0],  BillingAddress = billingAddress[0], ShippingAddress = shippingAddress[0], DateTimeCreated = DateTime.Now },
                    new Orders { PurchaseOrderNo = "P00000002", CustomerNo = "C00000001", Invoice = invoices[1],  OrderStatus = statusCodes[0],  BillingAddress = billingAddress[0], ShippingAddress = shippingAddress[0], DateTimeCreated = DateTime.Now }
                };
                purchases.ForEach(x => context.Orders.Add(x));

                context.SaveChanges();

                var items = new List<OrderedItems>()
                {
                    new OrderedItems { ItemEAN = "CRN00000001", ItemDesc = "Poor quality fake faux leather cover loose enough to fit any mobile device.", ItemName = "Wrap It and Hope Cover", ItemQuanity = 2, ItemUnitPrice = 5.99, Order = purchases[0] },
                    new OrderedItems { ItemEAN = "CRN00000002", ItemDesc = "Excellant quality festive wool jumper.", ItemName = "Santa Christmas Jumper", ItemQuanity = 10, ItemUnitPrice = 12.99, Order = purchases[0] },
                    new OrderedItems { ItemEAN = "CRN00000001", ItemDesc = "Poor quality fake faux leather cover loose enough to fit any mobile device.", ItemName = "Wrap It and Hope Cover", ItemQuanity = 5, ItemUnitPrice = 5.99, Order = purchases[1] },
                    new OrderedItems { ItemEAN = "CRN00000002", ItemDesc = "Excellant quality festive wool jumper.", ItemName = "Santa Christmas Jumper", ItemQuanity = 10, ItemUnitPrice = 12.99, Order = purchases[1] },
                };
                items.ForEach(x => context.OrderedItems.Add(x));

                base.Seed(context);
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
