﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_EFModel.Tables
{
    public class OrderedItems
    {
        //The database index of the ordered item record.
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int PK_ItemID { get; set; }

        //The EAN of the item being ordered
        [Required]
        public virtual string ItemEAN { get; set; }

        //The name of the item ordered by the customer. NOTE: IS THIS REQUIRED?
        [Required]
        public virtual string ItemName { get; set; }

        //The description of the item ordered by the customer. NOTE: IS THIS REQUIRED?
        public virtual string ItemDesc { get; set; }

        //The number of items ordered by the customer
        [Required]
        public virtual int ItemQuanity { get; set; }

        //The price of a single unit of the ordered item (£)
        [Required]
        public virtual double ItemUnitPrice { get; set; }

        //The Foriegn key from the related order record.
        [Required]
        public virtual int FK_OrderID { get; set; }

        //The linked order object.
        public virtual Orders Order { get; set; }
    }
}
