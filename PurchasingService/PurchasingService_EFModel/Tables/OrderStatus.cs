﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_EFModel.Tables
{
    public class OrderStatus
    {
        //The order status code.
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public virtual int PK_StatusCode { get; set; }

        //The description of the status code.
        [Required]
        public virtual string StatusDescription { get; set; }
    }
}
