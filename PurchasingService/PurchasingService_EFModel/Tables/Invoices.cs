﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_EFModel.Tables
{
    public class Invoices
    {
        //The database index of the invoice table.
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int PK_InvoiceID { get; set; }

        //The invoice number. Example format (I#### ####)
        [Required]
        public virtual string InvoiceNo { get; set; }

        //The date and time when the invoice was generated.
        [Required]
        public virtual DateTime InvoiceDate { get; set; }

        //A boolean that represents if a customer has recieved the following invoice.
        [Required]
        public virtual bool SentToCustomer { get; set; }

        //A list of all the ordered items associated with the current invoice.
        public virtual ICollection<Orders> OrderList { get; set; }
    }
}
