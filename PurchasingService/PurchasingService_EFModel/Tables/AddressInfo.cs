﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_EFModel.Tables
{
    public class AddressInfo
    {
        //The database index of the address record.
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int PK_AddressNo { get; set; }

        //Address line 1.
        [Required]
        public virtual string Address1 { get; set; }

        //Address line 2.
        [Required]
        public virtual string Address2 { get; set; }

        //Address line 3.
        public virtual string Address3 { get; set; }

        //City of shipping or billing address.
        public virtual string City { get; set; }

        //County of shipping or billing address
        public virtual string County { get; set; }

        //Country of shipping or billing address
        [Required]
        public virtual string Country { get; set; }

        //The post code of the shipping or billing address
        [Required]
        public virtual string PostCode { get; set; }
    }
}