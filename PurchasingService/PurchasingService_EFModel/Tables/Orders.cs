﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_EFModel.Tables
{
    public class Orders
    {
        //Database index for order record.
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual int PK_OrderID { get; set; }

        //P.O.# of the order made by a customer. Example format: P#### ####
        [Required]
        public virtual string PurchaseOrderNo { get; set; }

        //The customer number of the customer making the order. Example format: C#### ####
        [Required]
        public virtual string CustomerNo { get; set; }

        #region Relationships
        //The status code of the current order. 1 = placed, 2 = processed to billing, 3 = sent to dispatch, 4 = dispatched, 5 = complete, 99 = cancelled
        [Required]
        public virtual int FK_StatusCode { get; set; }

        //The order status object of the current order.
        public virtual OrderStatus OrderStatus { get; set; }

        //A list of all items ordered in the current order.
        public virtual ICollection<OrderedItems> OrderedItems { get; set; }

        //The database index of the shipping address record.
        [Required]
        public virtual int FK_ShippingAddressID { get; set; }

        //The shipping address object.
        public virtual AddressInfo ShippingAddress { get; set; }

        //The database index of the billing address record.
        [Required]
        public virtual int FK_BillingAddressID { get; set; }

        //The billing address object.
        public virtual AddressInfo BillingAddress { get; set; }

        //Database index of the generated invoice for the current order.
        public virtual int? FK_InvoiceID { get; set; }

        //The invoice object of the current order.
        public virtual Invoices Invoice { get; set; }

        [Required]
        public virtual DateTime DateTimeCreated { get; set; }
        #endregion
    }
}
