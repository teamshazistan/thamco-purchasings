﻿using AutoMapper;
using PurchasingService_Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurchasingService.Controllers
{
    public class OrderController : ApiController
    {
        private AbstractRepository repo;
        private MapperConfiguration mConfig;
        public OrderController()
        {
            //Identify if customer or staff.
            
            //repo = new StaffRepository();
            repo = new CustomerRepository();
            var v = repo.GetOrdersFromCustomerNo("C0000 0001");
        }

        // GET: api/Order/"C1000 0012"
        public IEnumerable<string> GetOrder(string customerNo)
        {
            var returnedList = repo.GetOrdersFromCustomerNo(customerNo);
            return null;
            //return returnedList.Select(x => new )
        }

        // GET: api/Order/5
        public string GetOrder(int id)
        {
            return "value";
        }

        // POST: api/Order
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Order/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Order/5
        public void Delete(int id)
        {
        }
    }
}
