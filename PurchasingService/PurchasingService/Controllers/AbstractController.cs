﻿using AutoMapper;
using PurchasingService_Repository.DTOs.Invoices;
using PurchasingService_Repository.DTOs.Orders;
using PurchasingService_Repository.EFClones;
using PurchasingService_Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurchasingService.Controllers
{
    public class AbstractController : ApiController
    {
        protected AbstractRepository repo;

        protected IMapper iMapper;
        protected AutoMapper.MapperConfiguration mapperConfig;

        public AbstractController()
        {
            //if (customerUser)
            //    repo = new CustomerRepository();
            //else
            //if (staffUser)
            //    repo = new StaffRepository();

            mapperConfig = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<_Orders, OrdersDTO>()
                .ForMember(dest => dest.OrderStatusNo, opts => opts.ResolveUsing(src => src.OrderStatus == null ? -99 : src.OrderStatus.PK_StatusCode))
                .ForMember(dest => dest.OrderStatusDescription, opts => opts.ResolveUsing(src => src.OrderStatus == null ? "" : src.OrderStatus.StatusDescription))
                .ForMember(dest => dest.InvoiceNo, opts => opts.ResolveUsing(src => src.Invoice == null ? "" : src.Invoice.InvoiceNo));
                cfg.CreateMap<_Invoices, InvoiceDTO>();
            });
            iMapper = mapperConfig.CreateMapper();
        }
    }
}
