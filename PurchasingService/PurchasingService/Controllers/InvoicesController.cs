﻿using PurchasingService_Repository.DTOs.Invoices;
using PurchasingService_Repository.EFClones;
using PurchasingService_Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurchasingService.Controllers
{
    public class InvoicesController : AbstractController
    {
        public InvoicesController() { }

        #region Get
        
        [HttpGet]
        [Route("api/Invoices")]
        public IHttpActionResult GetInvoices(string orderNo = "", string invoiceNo = "", string customerNo = "") //Get customerNo from token.
        {
            try
            {
                if ((orderNo != "" || invoiceNo != "") && customerNo != "")
                {
                    repo = new CustomerRepository();
                    IEnumerable<_Invoices> filteredInvoices = repo.GetInvoicesFiltered(orderNo, invoiceNo, customerNo);
                    if (filteredInvoices != null && filteredInvoices.Count() > 0)
                        return Content(HttpStatusCode.OK, filteredInvoices.Select(x => iMapper.Map<_Invoices, InvoiceDTO>(x)).ToList());
                    return Content(HttpStatusCode.NotFound, "");
                }
                return Content(HttpStatusCode.BadRequest, "");
            }
            catch (SqlException ex)
            {
                return Content(HttpStatusCode.ServiceUnavailable, "");
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "");
            }
        }

        //Staff only
        [HttpGet]
        [Route("api/PendingInvoices")]
        public IHttpActionResult GetPendingInvoices()
        {
            try
            {
                repo = new StaffRepository();
                IEnumerable<_Invoices> pendingInvoices = ((StaffRepository)repo).GetPendingInvoices();
                if(pendingInvoices != null && pendingInvoices.Count() > 0)
                    return Content(HttpStatusCode.OK, pendingInvoices.Select(x => iMapper.Map<_Invoices, InvoiceDTO>(x)).ToList());
                return Content(HttpStatusCode.NotFound, "");
            }
            catch (SqlException ex)
            {
                return Content(HttpStatusCode.ServiceUnavailable, "");
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "");
            }
        }
        #endregion

        #region Post
        [HttpPost]
        [Route("api/SendInvoice")]
        public IHttpActionResult SendInvoice(InvoiceStatusUpdateDTO invoiceStatusUpdate)
        {
            try
            {
                repo = new StaffRepository();
                _Invoices updatedInvoice = ((StaffRepository)repo).UpdateInvoiceSentStatus(invoiceStatusUpdate);
                if (updatedInvoice != null)
                    return Content(HttpStatusCode.OK, iMapper.Map<_Invoices, InvoiceDTO>(updatedInvoice));
                return Content(HttpStatusCode.NotFound, "");
            }
            catch (SqlException ex)
            {
                return Content(HttpStatusCode.ServiceUnavailable, "");
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "");
            }
        }
        #endregion
    }
}
