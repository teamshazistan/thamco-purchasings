﻿using AutoMapper;
using PurchasingService_Repository.DTOs.Orders;
using PurchasingService_Repository.DTOs.Orders.NewOrders;
using PurchasingService_Repository.EFClones;
using PurchasingService_Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PurchasingService.Controllers
{
    public class OrdersController : AbstractController
    {
        public OrdersController() { }
       
        #region Gets
        [HttpGet]
        [Route("api/Orders")]
        public IHttpActionResult GetOrders(string orderNo = "", string customerNo = "") //Get customerNo from token.
        {
            try
            {
                if (orderNo != "" || customerNo != "")
                {
                    repo = new StaffRepository();
                    IEnumerable<_Orders> filteredOrders = repo.GetOrdersFiltered(orderNo, customerNo);
                    if (filteredOrders != null && filteredOrders.Count() > 0)
                        return Content(HttpStatusCode.OK, filteredOrders.Select(x => iMapper.Map<_Orders, OrdersDTO>(x)).ToList());
                    return Content(HttpStatusCode.NotFound, "");
                }
                return Content(HttpStatusCode.BadRequest, "");
            }
            catch (SqlException ex)
            {
                return Content(HttpStatusCode.ServiceUnavailable, "");
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "");
            }
        }
        #endregion


        #region Posts
        [HttpPost]
        [Route("api/Orders/New")]
        public IHttpActionResult AddNewOrder(NewOrderDTO newOrder)
        {
            try
            {
                if (newOrder != null)
                {
                    repo = new CustomerRepository();
                    _Orders newOrderRecord = ((CustomerRepository)repo).PlaceOrder(newOrder);
                    if (newOrderRecord != null)
                        return Content(HttpStatusCode.Created, (iMapper.Map<_Orders, OrdersDTO>(newOrderRecord)));
                    return Content(HttpStatusCode.NotFound, "");
                }
                return Content(HttpStatusCode.BadRequest, "");
            }
            catch (SqlException ex)
            {
                return Content(HttpStatusCode.ServiceUnavailable, "");
            }
            catch (InvalidCastException ex)
            {
                return Content(HttpStatusCode.InternalServerError, "");
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "");
            }
        }

        [HttpPost]
        [Route("api/Orders/StatusUpdate")]
        public IHttpActionResult UpdateOrderStatus(OrderStatusUpdateDTO updatedOrder)
        {
            try
            {
                if (updatedOrder != null)
                {
                    repo = new StaffRepository();
                    _Orders updatedOrderRecord = ((StaffRepository)repo).UpdateOrderStatus(updatedOrder);
                    if (updatedOrderRecord != null)
                        return Content(HttpStatusCode.Created, (iMapper.Map<_Orders, OrdersDTO>(updatedOrderRecord)));
                    return Content(HttpStatusCode.NotFound, "");
                }
                return Content(HttpStatusCode.BadRequest, "");
            }
            catch (SqlException ex)
            {
                return Content(HttpStatusCode.ServiceUnavailable, "");
            }
            catch (InvalidCastException ex)
            {
                return Content(HttpStatusCode.InternalServerError, "");
            }
            catch (Exception ex)
            {
                return Content(HttpStatusCode.BadRequest, "");
            }
        }
        #endregion
    }
}
