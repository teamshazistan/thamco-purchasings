﻿using PurchasingService_EFModel.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_Repository.EFClones
{
    public class _OrderedItems
    {
        //The database index of the ordered item record.
        public int PK_ItemID { get; set; }

        //The EAN of the item being ordered
        public string ItemEAN { get; set; }

        //The name of the item ordered by the customer. NOTE: IS THIS REQUIRED?
        public string ItemName { get; set; }

        //The description of the item ordered by the customer. NOTE: IS THIS REQUIRED?
        public string ItemDesc { get; set; }

        //The number of items ordered by the customer
        public int ItemQuantity { get; set; }

        //The price of a single unit of the ordered item (£)
        public double ItemUnitPrice { get; set; }

        public int FK_OrderID { get; set; }
    }
}
