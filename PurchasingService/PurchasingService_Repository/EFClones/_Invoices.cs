﻿using PurchasingService_EFModel.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_Repository.EFClones
{
    public class _Invoices
    {
        //The database index of the invoice table.
        public int PK_InvoiceID { get; set; }

        //The invoice number. Example format (I#### ####)
        public string InvoiceNo { get; set; }

        //The date and time when the invoice was generated.
        public DateTime InvoiceDate { get; set; }

        //A boolean that represents if a customer has recieved the following invoice.
        public bool SentToCustomer { get; set; }

        //A list of all the purchased items associated with the current invoice.
        public IEnumerable<_Orders> OrderList { get; set; }
    }
}
