﻿using PurchasingService_EFModel.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_Repository.EFClones
{
    public class _Orders
    {
        //Database index for order record.
        public int PK_OrderID { get; set; }

        //P.O.# of the order made by a customer. Example format: P#### ####
        public string PurchaseOrderNo { get; set; }

        //The customer number of the customer making the order. Example format: C#### ####
        public string CustomerNo { get; set; }

        public IEnumerable<_OrderedItems> OrderedItems { get; set; }

        //The status code of the current order. 1 = placed, 2 = processed to billing, 3 = sent to be dispatched, 4 = dispatched, 5 = complete, 99 = cancelled
        public int FK_StatusCode { get; set; }

        //The order status object of the current order.
        public _OrderStatus OrderStatus { get; set; }

        //Database index of the generated invoice for the current order.
        public int FK_InvoiceID { get; set; }

        //The invoice object of the current order.
        public _Invoices Invoice { get; set; }

        //The database index of the shipping address record.
        public int FK_ShippingAddressID { get; set; }

        //The shipping address object.
        public _AddressInfo ShippingAddress { get; set; }

        //The database index of the billing address record.
        public int FK_BillingAddressID { get; set; }

        //The billing address object.
        public _AddressInfo BillingAddress { get; set; }

        //The DateTime when the order was placed.
        public DateTime DateTimeCreated { get; set; }
    }
}
