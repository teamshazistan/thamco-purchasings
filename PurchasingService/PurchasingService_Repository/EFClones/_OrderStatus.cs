﻿using PurchasingService_EFModel.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_Repository.EFClones
{
    public class _OrderStatus
    {
        //The order status code.
        public int PK_StatusCode { get; set; }

        //The description of the status code.
        public string StatusDescription { get; set; }
    }
}
