﻿using PurchasingService_Repository.EFClones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_Repository.DTOs.AddressInfo
{
    public class AddressInfoDTO
    {
        //Address line 1.
        public string Address1 { get; set; }

        //Address line 2.
        public string Address2 { get; set; }

        //Address line 3.
        public string Address3 { get; set; }

        //City of shipping or billing address.
        public string City { get; set; }

        //County of shipping or billing address
        public string County { get; set; }

        //Country of shipping or billing address
        public string Country { get; set; }

        //The post code of the shipping or billing address
        public string PostCode { get; set; }

        //A boolean that represents if the current address is billing (1) or shipping (0).
        public bool BillingAddressBool { get; set; }

        public AddressInfoDTO(_AddressInfo efCloneObj)
        {
            Address1 = efCloneObj.Address1 ?? "";
            Address2 = efCloneObj.Address2 ?? "";
            Address3 = efCloneObj.Address3 ?? "";
            BillingAddressBool = efCloneObj.BillingAddressBool;
            City = efCloneObj.City ?? "";
            County = efCloneObj.County ?? "";
            Country = efCloneObj.Country ?? "";
            PostCode = efCloneObj.PostCode ?? "";
        }
    }
}
