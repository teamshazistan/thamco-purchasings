﻿using PurchasingService_Repository.EFClones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_Repository.DTOs.OrderStatus
{
    public class OrderStatusDTO
    {
        public int StatusCode { get; set; }

        public string StatusDescription { get; set; }

        public OrderStatusDTO(_OrderStatus efCloneObj)
        {
            this.StatusCode = efCloneObj.PK_StatusCode;
            this.StatusDescription = efCloneObj.StatusDescription;
        }
    }
}
