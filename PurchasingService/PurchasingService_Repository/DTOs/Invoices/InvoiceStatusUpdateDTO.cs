﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_Repository.DTOs.Invoices
{
    public class InvoiceStatusUpdateDTO
    {
        public string InvoiceNo { get; set; }
        public bool SentToCustomer { get; set; }
    }
}
