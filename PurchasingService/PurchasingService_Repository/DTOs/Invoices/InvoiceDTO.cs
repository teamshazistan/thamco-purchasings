﻿using PurchasingService_Repository.DTOs.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_Repository.DTOs.Invoices
{
    public class InvoiceDTO
    {
        //The invoice number of the invoice record.
        public string InvoiceNo { get; set; }

        //The date and time when the invoice was generated.
        public DateTime InvoiceDate { get; set; }

        //A boolean that represents if a customer has recieved the following invoice.
        public bool SentToCustomer { get; set; }

        //A list of all the purchased items associated with the current invoice.
        public IEnumerable<OrdersDTO> OrderList { get; set; }
    }
}
