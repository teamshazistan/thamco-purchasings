﻿using PurchasingService_Repository.EFClones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_Repository.DTOs.OrderedItems
{
    public class OrderedItemDTO
    {
        public string ItemCRN { get; set; }
        public string ItemName { get; set; }
        public string ItemDesc { get; set; }
        public int ItemQuantity { get; set; }
        public double ItemUnitPrice { get; set; }

        public OrderedItemDTO(_OrderedItems efCloneObj)
        {
            this.ItemCRN = efCloneObj.ItemCRN;
            this.ItemName = efCloneObj.ItemName;
            this.ItemDesc = efCloneObj.ItemDesc;
            this.ItemQuantity = efCloneObj.ItemQuanity;
            this.ItemUnitPrice = efCloneObj.ItemUnitPrice;
        }
    }
}
