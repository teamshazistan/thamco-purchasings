﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_Repository.DTOs.Orders
{
    public class OrderStatusUpdateDTO
    {
        public string PurchaseOrderNo { get; set; }
        public int NewStatusCode { get; set; }
    }
}
