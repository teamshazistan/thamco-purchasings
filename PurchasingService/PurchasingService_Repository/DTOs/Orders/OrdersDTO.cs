﻿using PurchasingService_Repository.DTOs.Invoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_Repository.DTOs.Orders
{
    //The DTO object converted to JSON when user requests order objects.
    public class OrdersDTO
    {
        public string PurchaseOrderNo { get; set; }
        public string CustomerNo { get; set; }
        public int OrderStatusNo { get; set; }
        public string OrderStatusDescription { get; set; }

        public IEnumerable<OrderedItemsDTO> OrderedItems { get; set; }

        //The billing address object.
        public OrderAddressDTO BillingAddress { get; set; }

        //The shipping address object.
        public OrderAddressDTO ShippingAddress { get; set; }

        public string InvoiceNo { get; set; }
    }
}
