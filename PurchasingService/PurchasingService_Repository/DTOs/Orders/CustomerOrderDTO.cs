﻿using PurchasingService_Repository.DTOs.Invoices;
using PurchasingService_Repository.DTOs.OrderedItems;
using PurchasingService_Repository.DTOs.OrderStatus;
using PurchasingService_Repository.EFClones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_Repository.DTOs.Orders
{
    public class CustomerOrderDTO
    {
        public string PurchaseOrderNo { get; set; }

        public string CustomerNo { get; set; }

        public List<OrderedItemDTO> OrderedItems { get; set; }

        public OrderStatusDTO OrderStatus { get; set; }

        public InvoiceDTO Invoice { get; set; }

        public CustomerOrderDTO(_Orders efCloneObj)
        {
            this.PurchaseOrderNo = efCloneObj.PurchaseOrderNo;
            this.CustomerNo = efCloneObj.CustomerNo;
            this.OrderedItems = efCloneObj.OrderedItems.Select(x => new OrderedItemDTO(x)).ToList();
            this.OrderStatus = new OrderStatusDTO(efCloneObj.OrderStatus);
            this.Invoice = new InvoiceDTO(efCloneObj.Invoice);
        }
    }
}
