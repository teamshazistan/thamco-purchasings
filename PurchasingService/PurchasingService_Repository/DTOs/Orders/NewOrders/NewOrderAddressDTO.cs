﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurchasingService_Repository.DTOs.Orders.NewOrders
{
    public class NewOrderAddressDTO
    { 
        //Address line 1.
        public string Address1 { get; set; }

        //Address line 2.
        public string Address2 { get; set; }

        //Address line 3.
        public string Address3 { get; set; }

        //City of shipping or billing address.
        public string City { get; set; }

        //County of shipping or billing address
        public string County { get; set; }

        //Country of shipping or billing address
        public string Country { get; set; }

        //The post code of the shipping or billing address
        public string PostCode { get; set; }

        //Used to quickly determine if the shipping address of an order is exactly the same as the billing address. Reduces duplicated data.
        public string ToString => String.Format("{0},{1},{2},{3},{4},{5},{6}", Address1 ?? "", Address2 ?? "", Address3 ?? "", City ?? "", County ?? "", Country ?? "", PostCode ?? "");
    }
}