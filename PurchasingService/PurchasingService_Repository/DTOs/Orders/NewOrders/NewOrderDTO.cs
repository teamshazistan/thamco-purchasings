﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PurchasingService_Repository.DTOs.Orders.NewOrders
{
    public class NewOrderDTO
    {
        //The customer number of the customer making the order. Example format: C#### ####
        public string CustomerNo { get; set; }

        //A list of all items ordered in the current order.
        public List<NewOrderedItemsDTO> OrderedItems { get; set; }

        //The billing address object.
        public NewOrderAddressDTO BillingAddress { get; set; }

        //The shipping address object.
        public NewOrderAddressDTO ShippingAddress { get; set; }
    }
}