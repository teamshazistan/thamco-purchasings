﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_Repository.DTOs.Orders
{
    public class OrderAddressDTO
    {
        //Address line 1.
        public string Address1 { get; set; }

        //Address line 2.
        public string Address2 { get; set; }

        //Address line 3.
        public string Address3 { get; set; }

        //City of shipping or billing address.
        public string City { get; set; }

        //County of shipping or billing address
        public string County { get; set; }

        //Country of shipping or billing address
        public string Country { get; set; }

        //The post code of the shipping or billing address
        public string PostCode { get; set; }
    }
}
