﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_Repository.DTOs.Orders
{
    public class OrderedItemsDTO
    {
        public string ItemEAN { get; set; }
        public string ItemName { get; set; }
        public string ItemDesc { get; set; }
        public int ItemQuantity { get; set; }
        public double ItemUnitPrice { get; set; }
    }
}
