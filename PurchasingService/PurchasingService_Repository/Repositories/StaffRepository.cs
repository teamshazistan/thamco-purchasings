﻿using PurchasingService_EFModel.Tables;
using PurchasingService_Repository.DTOs.Invoices;
using PurchasingService_Repository.DTOs.Orders;
using PurchasingService_Repository.EFClones;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_Repository.Repositories
{
    public class StaffRepository : AbstractRepository
    {
        #region Orders
        /// <summary>
        /// Returns a list of orders made by a specified customer.
        /// </summary>
        /// <param name="customerNo"></param>
        /// <returns></returns>
        public override IEnumerable<_Orders> GetOrdersFiltered(string orderNo = "", string customerNo = "")
        {
            try
            {
                if (orderNo == null)
                    orderNo = "";
                if (customerNo == null)
                    customerNo = "";

                IQueryable<PurchasingService_EFModel.Tables.Orders> returnedOrders = db.Orders.AsQueryable();
                if (orderNo != "")
                    returnedOrders = returnedOrders.Where(x => x.PurchaseOrderNo == orderNo).AsQueryable();
                if (customerNo != "")
                    returnedOrders = returnedOrders.Where(x => x.CustomerNo == customerNo).AsQueryable();
                
                return returnedOrders.AsEnumerable().Select(x => iMapper.Map<Orders, _Orders>(x)).ToList();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Updates the status code of the order linked to the provided PurchaseOrderNumber.
        /// </summary>
        /// <param name="updatedOrder"></param>
        /// <returns></returns>
        public _Orders UpdateOrderStatus(OrderStatusUpdateDTO updatedOrder)
        {
            try
            {
                if (updatedOrder != null)
                {
                    var statusCodeRecord = db.OrderStatus.Where(x => x.PK_StatusCode == updatedOrder.NewStatusCode).FirstOrDefault();
                    if (statusCodeRecord != null)
                    {
                        var orderRecord = db.Orders.Where(x => x.PurchaseOrderNo == updatedOrder.PurchaseOrderNo).FirstOrDefault();
                        if (orderRecord != null)
                        {
                            orderRecord.FK_StatusCode = updatedOrder.NewStatusCode;
                            db.SaveChanges();
                            return iMapper.Map<Orders, _Orders>(orderRecord);
                        }
                    }
                }
                return null;
            }
            catch (SqlException ex)
            {
                throw ex;
            }     
        }
        #endregion

        #region Invoices
        /// <summary>
        /// Returns a list of invoices associated with a specified purchase order number.
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public override IEnumerable<_Invoices> GetInvoicesFiltered(string orderNo = "", string invoiceNo = "", string customerNo = "")
        {
            try
            {
                if (orderNo == null)
                    orderNo = "";
                if (invoiceNo == null)
                    invoiceNo = "";
                if (customerNo == null)
                    customerNo = "";

                IQueryable<PurchasingService_EFModel.Tables.Invoices> returnedInvoices = db.Invoices.AsQueryable();
                if (customerNo != "")
                    returnedInvoices = returnedInvoices.Where(x => x.OrderList.Any(y => y.CustomerNo == customerNo)).AsQueryable();
                if (invoiceNo != "")
                    returnedInvoices = returnedInvoices.Where(x => x.InvoiceNo == invoiceNo).AsQueryable();
                if (orderNo != "")
                    returnedInvoices = returnedInvoices.Where(x => x.OrderList.Any(y => y.PurchaseOrderNo == orderNo)).AsQueryable();

                return returnedInvoices.AsEnumerable().Select(x => iMapper.Map<Invoices, _Invoices>(x)).ToList();
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Returns a list of all pending invoices (yet to be sent to customers).
        /// </summary>
        /// <returns></returns>
        public IEnumerable<_Invoices> GetPendingInvoices()
        {
            try
            {
                return db.Invoices.Where(x => !x.SentToCustomer).AsEnumerable().Select(x => iMapper.Map<Invoices, _Invoices>(x)).ToList();
            }
            catch(SqlException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Updates the status code of the Invoice linked to the provided InvoiceNo.
        /// </summary>
        /// <param name="updatedInvoice"></param>
        /// <returns></returns>
        public _Invoices UpdateInvoiceSentStatus(InvoiceStatusUpdateDTO updatedInvoice)
        {
            try
            {
                if (updatedInvoice != null)
                {
                    var invoiceRecord = db.Invoices.Where(x => x.InvoiceNo == updatedInvoice.InvoiceNo).FirstOrDefault();
                    if (invoiceRecord != null)
                    {
                        invoiceRecord.SentToCustomer = updatedInvoice.SentToCustomer;
                        db.SaveChanges();
                        return iMapper.Map<Invoices, _Invoices>(invoiceRecord);
                    }
                }
                return null;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
