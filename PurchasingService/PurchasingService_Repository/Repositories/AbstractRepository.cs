﻿using AutoMapper;
using PurchasingService_EFModel;
using PurchasingService_EFModel.Tables;
using PurchasingService_Repository.DTOs.Orders.NewOrders;
using PurchasingService_Repository.EFClones;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PurchasingService_Repository.Repositories
{
    public abstract class AbstractRepository
    {
        protected PurchasesDb db;
        protected IMapper iMapper;
        protected AutoMapper.MapperConfiguration mapperConfig;

        public AbstractRepository()
        {
            db = new PurchasesDb();
            mapperConfig = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Orders, _Orders>().MaxDepth(2);
                cfg.CreateMap<Invoices, _Invoices>().MaxDepth(2);
                cfg.CreateMap<AddressInfo, _AddressInfo>();
                cfg.CreateMap<OrderedItems, _OrderedItems>().MaxDepth(2);
                cfg.CreateMap<OrderStatus, _OrderStatus>();

                cfg.CreateMap<NewOrderAddressDTO, AddressInfo>();
                cfg.CreateMap<NewOrderedItemsDTO, OrderedItems>();
            });
            iMapper = mapperConfig.CreateMapper();
        }

        public abstract IEnumerable<_Orders> GetOrdersFiltered(string orderNo, string customerNo);
        public abstract IEnumerable<_Invoices> GetInvoicesFiltered(string orderNo, string invoiceNo, string customerNo);
    }
}
