﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PurchasingService_Repository.EFClones;
using System.Configuration;
using PurchasingService_EFModel.Tables;
using PurchasingService_Repository.DTOs.Orders.NewOrders;
using System.Text.RegularExpressions;
using PurchasingService_Repository.DTOs.Orders;
using System.Data.Entity.Core;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;

namespace PurchasingService_Repository.Repositories
{
    public class CustomerRepository : AbstractRepository
    {
        #region DataRetrieval

        #region Orders
        /// <summary>
        /// Returns a list of orders for the current customer.
        /// </summary>
        /// <param name="customerNo"></param>
        /// <returns></returns>
        public override IEnumerable<_Orders> GetOrdersFiltered(string orderNo = "", string customerNo = "")
        {
            try
            {
                if (orderNo == null)
                    orderNo = "";
                if (customerNo == null)
                    customerNo = "";

                //We do not want customers to retrieve all orders. So only return data if some sort of filter is being applied.
                if (orderNo != "" || customerNo != "")
                {
                    IQueryable<PurchasingService_EFModel.Tables.Orders> returnedOrders = db.Orders.AsQueryable();
                    if (orderNo != "")
                        returnedOrders = returnedOrders.Where(x => x.PurchaseOrderNo == orderNo).AsQueryable();
                    if (customerNo != "")
                        returnedOrders = returnedOrders.Where(x => x.CustomerNo == customerNo).AsQueryable();

                    return returnedOrders.AsEnumerable().Select(x => iMapper.Map<Orders, _Orders>(x)).ToList();
                }
                return null;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Invoices

        /// <summary>
        /// Returns a list of invoices associated with a specified purchase order number.
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public override IEnumerable<_Invoices> GetInvoicesFiltered(string orderNo = "", string invoiceNo = "", string customerNo = "")
        {
            if (orderNo == null)
                orderNo = "";
            if (invoiceNo == null)
                invoiceNo = "";
            if (customerNo == null)
                customerNo = "";

            try
            {
                //We do not want customers to retrieve all invoices. So only return data if some sort of filter is being applied. CustomerNo should NOT be empty.
                if ((orderNo != "" || invoiceNo != "") && customerNo != "")
                {
                    IQueryable<PurchasingService_EFModel.Tables.Invoices> returnedInvoices = db.Invoices.Where(x => x.SentToCustomer && x.OrderList.Any(y => y.CustomerNo == customerNo)).AsQueryable();
                    if (invoiceNo != "")
                        returnedInvoices = returnedInvoices.Where(x => x.InvoiceNo == invoiceNo).AsQueryable();
                    if (orderNo != "")
                        returnedInvoices = returnedInvoices.Where(x => x.OrderList.Any(y => y.PurchaseOrderNo == orderNo)).AsQueryable();

                    return returnedInvoices.AsEnumerable().Select(x => iMapper.Map<Invoices, _Invoices>(x)).ToList();
                }
                return null;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion


        #region DataSubmitting

        #region Orders

        public _Orders PlaceOrder(NewOrderDTO newOrder)
        {
            try
            {
                if (newOrder != null)
                {

                    //Check if billing address already exists in database
                    AddressInfo billingAddress = db.AddressInfo.Where(x => x.Address1 == newOrder.BillingAddress.Address1 && x.Address2 == newOrder.BillingAddress.Address2
                                                            && x.Address3 == newOrder.BillingAddress.Address3 && x.City == newOrder.BillingAddress.City && x.Country == newOrder.BillingAddress.Country
                                                            && x.County == newOrder.BillingAddress.County && x.PostCode == newOrder.BillingAddress.PostCode).FirstOrDefault();

                    //Check if shipping address already exists in database
                    AddressInfo shippingAddress = db.AddressInfo.Where(x => x.Address1 == newOrder.ShippingAddress.Address1 && x.Address2 == newOrder.ShippingAddress.Address2
                                                && x.Address3 == newOrder.ShippingAddress.Address3 && x.City == newOrder.ShippingAddress.City && x.Country == newOrder.ShippingAddress.Country
                                                && x.County == newOrder.ShippingAddress.County && x.PostCode == newOrder.ShippingAddress.PostCode).FirstOrDefault();

                    if (newOrder.BillingAddress.ToString() == newOrder.ShippingAddress.ToString() && billingAddress == null && shippingAddress == null)
                    {
                        billingAddress = iMapper.Map<NewOrderAddressDTO, AddressInfo>(newOrder.BillingAddress);
                        shippingAddress = billingAddress;
                    }
                    else
                    {
                        if (billingAddress == null)
                            billingAddress = iMapper.Map<NewOrderAddressDTO, AddressInfo>(newOrder.BillingAddress);
                        if (shippingAddress == null)
                            shippingAddress = iMapper.Map<NewOrderAddressDTO, AddressInfo>(newOrder.ShippingAddress);
                    }

                    List<OrderedItems> orderItems = newOrder.OrderedItems.Select(x => iMapper.Map<NewOrderedItemsDTO, OrderedItems>(x)).ToList();
                    PurchasingService_EFModel.Tables.Orders newEfOrder = new PurchasingService_EFModel.Tables.Orders()
                    {
                        CustomerNo = newOrder.CustomerNo,
                        FK_StatusCode = GetDefaultOrderStatusID(),
                        ShippingAddress = shippingAddress,
                        BillingAddress = billingAddress,
                        DateTimeCreated = DateTime.Now,
                        OrderedItems = orderItems,
                        PurchaseOrderNo = GenerateNewPurchaseOrderNo(),
                    };
                    db.Orders.Add(newEfOrder);
                    db.SaveChanges();
                    return GetOrdersFiltered(newEfOrder.PurchaseOrderNo, newEfOrder.CustomerNo).FirstOrDefault();
                }
                return null;
            }
            catch (DbEntityValidationException ex) //Thrown if values in table fields in invalid.
            {
                throw ex;
            }
            catch (DataException ex) //Thrown if a general SQL exception occurs. Unable to establish connection, table does not exist etc.
            {
                throw ex;
            }
        }
        #endregion

        #region OtherMethods
        private string GenerateNewPurchaseOrderNo()
        {
            //How do we handle an exception being thrown here?
            try
            {
                var latestOrder = db.Orders.OrderByDescending(x => x.DateTimeCreated).FirstOrDefault();
                string latestOrderNo = latestOrder.PurchaseOrderNo;
                int latestOrderNoDigits = Int32.Parse(Regex.Match(latestOrderNo, "\\d+").Value);
                int newOrderNoDigits = latestOrderNoDigits + 1;

                return String.Format("P{0:D8}", newOrderNoDigits);
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        /// <summary>
        /// Searches the purchasing database for the default order status code. If no status code can be found, a default value (-99) is returned. This value is considered
        /// an error code.
        /// </summary>
        /// <returns></returns>
        private int GetDefaultOrderStatusID()
        {
            int defaultStatusCode = 1;
            if (Int32.TryParse(ConfigurationManager.AppSettings["DefaultStatusCode"], out defaultStatusCode))
            {
                if (db.OrderStatus.Any(x => x.PK_StatusCode == defaultStatusCode))
                    return defaultStatusCode;
            }
            return -99;
        }

        #endregion

        #endregion


    }
}
