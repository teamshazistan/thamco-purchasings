﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PurchasingService_Repository.EFClones;
using PurchasingService_EFModel;
using System.Threading;
using PurchasingService_EFModel.Tables;
using System.Text.RegularExpressions;

namespace PurchasingService_Repository.Repositories
{
    public class InvoiceGenerationRepository
    {
        protected PurchasesDb db;
        public InvoiceGenerationRepository()
        {
            db = new PurchasesDb();
        }

        #region Invoice Generation
        /// <summary>
        /// Every 2 minutes, the database is searched for orders that require invoices to be generated. If found, invoices are generated. An invoice is generated when a series of orders exceeds £50
        /// or when an order has not been made by that customer in 5 minutes.
        /// </summary>
        public void InvoiceGenerationChecker()
        {
            while(true)
            {
                Dictionary<string, List<Orders>> invoicelessOrders = db.Orders.Where(x => x.FK_InvoiceID == null).OrderBy(x => x.DateTimeCreated).GroupBy(x => x.CustomerNo).ToDictionary(x => x.Key, x => x.ToList());

                foreach(string customer in invoicelessOrders.Keys)
                {
                    Orders previousOrder = null;
                    List<Orders> newInvoiceOrders = new List<Orders>(); //A list of all orders that will go into the new invoice.
                    DateTime upperTimeLimit = DateTime.MinValue;
                    foreach(Orders order in invoicelessOrders[customer])
                    {
                        newInvoiceOrders.Add(order);
                        //Checks if series of orders have exceeded £50 or not.
                        if (newInvoiceOrders.Sum(x => x.OrderedItems.Sum(y => y.ItemUnitPrice * y.ItemQuanity)) > 50)
                        {
                            GenerateInvoices(newInvoiceOrders);
                            newInvoiceOrders = new List<Orders>();
                        }
                        else
                        {
                            //Otherwise, check if no invoice has been generated within five minutes of the orders.

                            //Checks if the current order is the most recent, if so use DateTime.Now as the upperTimeLimit.
                            if (invoicelessOrders[customer].Last() == order)
                            {
                                upperTimeLimit = DateTime.Now;
                                previousOrder = order;
                            }
                            else //If order is not most recent, set the upperLimit to the current orders time
                                upperTimeLimit = order.DateTimeCreated;

                            if (previousOrder != null)
                            {
                                if ((upperTimeLimit - previousOrder.DateTimeCreated).TotalMinutes >= 5)
                                {
                                    GenerateInvoices(newInvoiceOrders);
                                    newInvoiceOrders = new List<Orders>();
                                }
                            }

                        }
                        previousOrder = order;
                    }
                }
                Thread.Sleep(120000); //2 minutes
            }
        }

        /// <summary>
        /// Creates Invoice record for the purchasing database and updates each order record.
        /// </summary>
        /// <param name="newInvoiceOrders"></param>
        private void GenerateInvoices(List<Orders> newInvoiceOrders)
        {
            Invoices newInvoice = new Invoices()
            {
                InvoiceDate = DateTime.Now,
                SentToCustomer = false,
                InvoiceNo = GenerateNewInvoiceNo()
            };
            db.Invoices.Add(newInvoice);
            foreach (Orders newInvoiceOrder in newInvoiceOrders)
            {
                newInvoiceOrder.Invoice = newInvoice;
            }
            db.SaveChanges();
        }
        #endregion

        #region OtherMethods
        private string GenerateNewInvoiceNo()
        {
            //How do we handle an exception being thrown here?
            try
            {
                var latestInvoice = db.Invoices.OrderByDescending(x => x.InvoiceDate).FirstOrDefault();
                string latestInvoiceNo = latestInvoice.InvoiceNo;
                int latestInvoiceNoDigits = Int32.Parse(Regex.Match(latestInvoiceNo, "\\d+").Value);
                int newInvoiceNoDigits = latestInvoiceNoDigits + 1;

                return String.Format("I{0:D8}", newInvoiceNoDigits);
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        #endregion
    }
}
